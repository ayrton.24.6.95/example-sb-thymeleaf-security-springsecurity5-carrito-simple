/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.edu.cibertec.HolaEcommerce.dao;

import com.edu.cibertec.HolaEcommerce.domain.Producto;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Stephanie
 */
public interface ProductoDao extends CrudRepository<Producto, Long>{
    
}
