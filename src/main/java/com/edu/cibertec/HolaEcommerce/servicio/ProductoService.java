/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.edu.cibertec.HolaEcommerce.servicio;

import com.edu.cibertec.HolaEcommerce.domain.Producto;
import java.util.List;

/**
 *
 * @author Stephanie
 */
public interface ProductoService {
    
    public List<Producto> listarProductos();
    
    
    public void guardar(Producto producto);
    
    
    public void eliminar(Producto producto);
    
    
    public Producto encontrarProducto(Producto producto);
    
}
