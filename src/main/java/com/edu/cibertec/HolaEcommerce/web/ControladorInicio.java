package com.edu.cibertec.HolaEcommerce.web;

import com.edu.cibertec.HolaEcommerce.domain.Producto;
import com.edu.cibertec.HolaEcommerce.servicio.ProductoService;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class ControladorInicio {

	@Autowired
	private ProductoService productoService;

	@GetMapping("/")
	public String inicio(Model model, @AuthenticationPrincipal User user) {
		// log.info("usuario que hizo login " + user);
		var productos = productoService.listarProductos();
		
		var agregarProductos = new Producto();

		model.addAttribute("productos", productos);
		model.addAttribute("agregarProductos",agregarProductos);
		
		var saldoTotal = 0D;
		for (var p : productos) {
			saldoTotal += p.getSaldo();
		}

		model.addAttribute("saldoTotal", saldoTotal);
		model.addAttribute("totalProductos", productos.size());

		log.info("ejecutando el controlador MVC");

		log.info("usuario que hizo login " + user);

		return "index";
	}

	@GetMapping("/agregar")
	public String agregar(Model model) {
		Producto producto = new Producto();
		model.addAttribute("producto", producto);
		return "/layout/agregarProducto";
	}

	@PostMapping("/guardar")
	public String guardar(@Valid Producto producto, Errors errores) {

		if (errores.hasErrors()) {
			return "modificar";
		}

		productoService.guardar(producto);
		return "redirect:/";

	}

	@GetMapping("/editar/{idProducto}")
	public String editar(Producto producto, Model model) {
		producto = productoService.encontrarProducto(producto);
		model.addAttribute("producto", producto);
		return "modificar";

	}

	@GetMapping("/eliminar/{idProducto}")
	public String eliminar(Producto producto) {
		productoService.eliminar(producto);
		return "redirect:/";
	}

}
