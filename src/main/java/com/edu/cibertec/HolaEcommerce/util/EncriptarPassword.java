/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.edu.cibertec.HolaEcommerce.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Stephanie
 */
public class EncriptarPassword {
    public static void main(String[] args) {
               
        var password = "123";
        
        System.out.println("pasword:" + password);
        System.out.println("pasword encriptar:" + encriptarPassword(password));
                
    }
    
    public static String encriptarPassword(String password){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        
        return encoder.encode(password) ;
    }
}
