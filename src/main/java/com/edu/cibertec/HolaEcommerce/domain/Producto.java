/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.edu.cibertec.HolaEcommerce.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Stephanie
 */

@Data
@Entity
@Table(name = "producto")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1l;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @Column(name = "id_producto")
	private Long idProducto;

	@NotEmpty
	// @Column(name = "nombre")
	private String nombre;

	@NotEmpty
	// @Column(name = "descripcion")
	private String descripcion;

	@NotNull
	// @Column(name = "cantidad")
	private int cantidad;

	@NotEmpty
	// @Column(name = "tipo")
	private String tipo;

	private Double saldo;

}
